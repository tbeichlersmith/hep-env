# hep-env Software Building and Running Environment

Docker build context and bash environment script for developing and running HEP software tools.

**Disclaimer**: This project is still in Beta - no stability is promised.

## Motivation

High Energy Physics (HEP) is a very complicated field in which several layers of independently complicated software tools are used.
The requirement of these software tools is a huge barrier-to-entry because new researchers need to either
obtain access to an environment already constructed for them (e.g. by connecting to a Univerity cluster) or
spend the days, weeks, or even months necessary to install all the software on their personal computers 
(which may not even be able to support the softwares they are interested in using).

For this reason, packaging the software dependencies into a container image that can be used to develop and run other HEP software is crucial.
This effectively reduces the required software on a user's computer to a _single_ dependency: a container runner (e.g. docker or singularity).

The benefits of this approach are multifaceted:
- As described above, this approach lowers the barrier-to-entry for new researchers.
- More computers can be supported, almost any computer that can run docker or singularity can run a container launched from these images.
- Large-scale production is a simple step away. Instead of being forced to build a new container image from scratch or 
  construct a cluster with all of the necessary dependencies, a group can simply create a new image _on top_ of this image that contains their dependencies.

## Usage

This build context also contains an environment script to help users interface with a container launched from this image.
After installing [docker](https://docs.docker.com/get-docker/) or [singularity](https://sylabs.io/singularity/) and 
downloading [hep-env.sh](hep-env.sh) to your computer, 
using HEP tools is as simple as downloading a new container image.

```bash
source hep-env.sh
hep root
```

Use `hep help` to see a full list of the sub-commands for the `hep` command.

## F.A.Q.

### Isn't this SlOw?

**No**. Opening a container and running a command inside of it is virtually indistinguishable
from running the command "on bare metal". Don't believe me? Give it a try! If you do notice a difference,
you can always delete your container running software and go back to the old-fashioned way.
