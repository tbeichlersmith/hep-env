
FROM ubuntu:20.04
LABEL ubuntu.version="20.04"
MAINTAINER Tom Eichlersmith <eichl008@umn.edu>

###############################################################################
# Source-Code Downloading Method
#   mkdir src && ${__wget} <url-to-tar.gz-source-archive> | ${__untar}
#
#   Adapted from acts-project/machines
###############################################################################
ENV __wget wget -q -O -
ENV __untar tar -xz --strip-components=1 --directory src
ENV __prefix /usr/local

# All init scripts in this directory will be run upon entry into container
ENV __hep_env_script_d__ /etc/hep-container-end.d
RUN mkdir ${__hep_env_script_d__}

# this directory is where folks should "install" code compiled with the container
ENV EXTERNAL_INSTALL_DIR /externals
ENV PATH="${EXTERNAL_INSTALL_DIR}/bin:${PATH}"
ENV LD_LIBRARY_PATH="${EXTERNAL_INSTALL_DIR}/lib"
ENV PYTHONPATH="${EXTERNAL_INSTALL_DIR}/lib:${EXTERNAL_INSTALL_DIR}/python"
ENV CMAKE_PREFIX_PATH="${EXTERNAL_INSTALL_DIR}" 

#run environment setup when docker container is launched and decide what to do from there
COPY ./entry.sh /etc/
RUN chmod 755 /etc/entry.sh
ENTRYPOINT ["/etc/entry.sh"]
